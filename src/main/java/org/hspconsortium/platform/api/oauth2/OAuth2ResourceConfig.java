package org.hspconsortium.platform.api.oauth2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.security.oauth2.provider.authentication.TokenExtractor;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Configuration
@EnableResourceServer
public class OAuth2ResourceConfig extends ResourceServerConfigurerAdapter {

    private static final String SECURITY_MODE_OPEN = "open";

    private static final String SECURITY_MODE_SECURED = "secured";

    private TokenExtractor tokenExtractor = new BearerTokenExtractor();

    @Value("${hsp.platform.api.security.mode}")
    private String securityMode;

    @Bean
    public AccessTokenConverter accessTokenConverter() {
        return new ScopeAsStringAccessTokenConverter();
    }

    @Bean
    public RemoteTokenServices remoteTokenServices(
            final @Value("${hsp.platform.authorization.tokenCheckUrl}") String tokenCheckUrl,
            final @Value("${hsp.platform.api.oauth2.clientId}") String clientId,
            final @Value("${hsp.platform.api.oauth2.clientSecret}") String clientSecret) {
        final RemoteTokenServices remoteTokenServices = new RemoteTokenServices();
        remoteTokenServices.setCheckTokenEndpointUrl(tokenCheckUrl);
        remoteTokenServices.setClientId(clientId);
        remoteTokenServices.setClientSecret(clientSecret);
        remoteTokenServices.setAccessTokenConverter(accessTokenConverter());
        return remoteTokenServices;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        switch (securityMode) {
            case (SECURITY_MODE_OPEN):
                http.csrf().disable();
                http
                        .authorizeRequests()
                        .anyRequest().permitAll();
                break;
            case (SECURITY_MODE_SECURED):
                http
                        .addFilterAfter(new OncePerRequestFilter() {
                            @Override
                            protected void doFilterInternal(HttpServletRequest request,
                                                            HttpServletResponse response, FilterChain filterChain)
                                    throws ServletException, IOException {
                                // We don't want to allow access to a resource with no token so clear
                                // the security context in case it is actually an OAuth2Authentication
                                if (tokenExtractor.extract(request) == null) {
                                    SecurityContextHolder.clearContext();
                                }
                                filterChain.doFilter(request, response);
                            }
                        }, AbstractPreAuthenticatedProcessingFilter.class);
                http
                        .authorizeRequests()
                        .antMatchers(
                                "/", "/health",
                                "/data", "/data/metadata",
                                "/smart", "/smart/Launch",
                                "/terminology/**").permitAll()
                        // This level of security says that any other requests (all requests for FHIR resources)
                        // must be authenticated.  It does not determine if the user has access to the specific
                        // data according to scope and user role. That more granular level of provisioning should
                        // be handled by an interceptor
                        .anyRequest().authenticated()
                        .and().addFilterAfter(csrfHeaderFilter(), CsrfFilter.class)
                ;
                break;
            default:
                throw new RuntimeException("Security mode must be either open or secured");
        }
    }

    private Filter csrfHeaderFilter() {
        return new OncePerRequestFilter() {
            @Override
            protected void doFilterInternal(HttpServletRequest request,
                                            HttpServletResponse response, FilterChain filterChain)
                    throws ServletException, IOException {
                CsrfToken csrf = (CsrfToken) request
                        .getAttribute(CsrfToken.class.getName());
                if (csrf != null) {
                    Cookie cookie = WebUtils.getCookie(request, "XSRF-TOKEN");
                    String token = csrf.getToken();
                    if (cookie == null
                            || token != null && !token.equals(cookie.getValue())) {
                        cookie = new Cookie("XSRF-TOKEN", token);
                        cookie.setPath("/");
                        response.addCookie(cookie);
                    }
                }
                filterChain.doFilter(request, response);
            }
        };
    }
}
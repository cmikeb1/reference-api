package org.hspconsortium.platform.api.oauth2;

import com.google.common.collect.Maps;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

public class ScopeAsStringAccessTokenConverterTest {

    @Test
    public void testExtractAuthentication_empty() {
        Map<String, ?> before = Maps.newHashMap();
        Map after = ScopeAsStringAccessTokenConverter.convertScopeStringToCollection(before);
        // same map
        Assert.assertTrue(before == after);
    }

    @Test
    public void testExtractAuthentication_String() {
        Map<String, Object> before = Maps.newHashMap();
        before.put(ScopeAsStringAccessTokenConverter.SCOPE, "1 2 3");
        Map after = ScopeAsStringAccessTokenConverter.convertScopeStringToCollection(before);
        // same map
        Collection<String> scopes = (Collection<String>)after.get(ScopeAsStringAccessTokenConverter.SCOPE);
        Assert.assertEquals(scopes.size(), 3);
        Assert.assertTrue(scopes.contains("1"));
        Assert.assertTrue(scopes.contains("2"));
        Assert.assertTrue(scopes.contains("3"));
    }

    @Test
    public void testExtractAuthentication_Collection() {
        Map<String, Object> before = Maps.newHashMap();
        before.put(ScopeAsStringAccessTokenConverter.SCOPE, Arrays.asList("1", "2", "3"));
        Map after = ScopeAsStringAccessTokenConverter.convertScopeStringToCollection(before);
        // same map
        Assert.assertTrue(before == after);
    }
}

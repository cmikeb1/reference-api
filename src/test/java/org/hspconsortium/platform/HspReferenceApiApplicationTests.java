package org.hspconsortium.platform;

import org.hspconsortium.platform.api.HspReferenceApiApplication;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HspReferenceApiApplication.class)
@Ignore
public class HspReferenceApiApplicationTests {

	@Test
	public void contextLoads() {
		Assert.assertTrue(true);
	}

}

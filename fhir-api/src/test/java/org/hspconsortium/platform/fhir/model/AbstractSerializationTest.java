/*
 * #%L
 * Healthcare Services Consortium Platform FHIR Server
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package org.hspconsortium.platform.fhir.model;

import org.springframework.util.SerializationUtils;

import static junit.framework.Assert.assertTrue;

abstract public class AbstractSerializationTest {

    public void testIsSerializable(Object myObject) {
        byte[] bytes = SerializationUtils.serialize(myObject);
        assertTrue(bytes.length > 0);

        Object obj = SerializationUtils.deserialize(bytes);
        assertTrue(obj != null);
    }
}
